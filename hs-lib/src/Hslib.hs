module Hslib where

import Control.Monad ( (<=<) )
import Foreign.C ( CString, newCString )

-- | CHANGE HERE
import           Your.EntryPoint ( launch )

foreign export ccall launchit :: CString -> IO ()

foreign import ccall "android_info" androidInfo   :: CString -> IO ()
foreign import ccall "android_warn" androidWarn   :: CString -> IO ()
foreign import ccall "android_error" androidError :: CString -> IO ()

launchit = launchit'

launchit' :: CString -> IO ()
launchit' _ = launch (androidInfo', androidWarn', androidError') where
    androidInfo'  = androidInfo  <=< newCString
    androidWarn'  = androidWarn  <=< newCString
    androidError' = androidError <=< newCString

