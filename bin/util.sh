set -eu
set -o pipefail

echo-y () { echo y; }

sha1sumc () {
    sha1sum -c
}

acat-gpg-cmd () {
    local gpgdir=$1; shift
    cmd gpg --homedir="$gpgdir" "$@"
}

acat-get-android-sdk () {
    local sdkdir=$1;              shift
    local sdktoolsurl=$1;         shift
    local javahome=$1;            shift
    local androidapiversion=$1;   shift
    local buildtoolsversion=$1;   shift
    local sdkplatformtoolsurl=$1; shift
    local sdklicensestring=$1;    shift

    xport JAVA_HOME "$javahome"

    local base1=$(basename "$sdktoolsurl")
    local base2=$(basename "$sdkplatformtoolsurl")

    cmd safe-rm-dir-allow-absolute "$sdkdir"
    mkchd "$sdkdir"

    cmd curl -Lo "$base1" "$sdktoolsurl"
    cmd curl -Lo "$base2" "$sdkplatformtoolsurl"
    cmd unzip "$base1"
    cmd unzip "$base2"

    if [ -n "$sdklicensestring" ]; then
        mkd licenses
        redirect-out licenses/android-sdk-license echo "$sdklicensestring"
    fi

    cmd tools/bin/sdkmanager "platforms;android-$androidapiversion"
    cmd tools/bin/sdkmanager "build-tools;$buildtoolsversion"
}

acat-get-android-ndk () {
    local ndkrootdir=$1;    shift
    local ndkstubdir=$1;    shift
    local ndkremote=$1;     shift
    local ndkremotesha1=$1; shift

    local file=ndk.zip
    mkchd "$ndkrootdir"
    if [ -d "$ndkstubdir" ]; then
        error "$ndkrootdir/$ndkstubdir exists, exiting."
    fi
    cmd curl -o "$file" "$ndkremote"
    fun sha1sumc <<< "$ndkremotesha1 $file"
    cmd unzip "$file"
    cmd rm -f "$file"
}

cabal-install-normal () {
    local p
    if [ "$cabalnormalprofiling" = yes ]; then p=-p
    else p=; fi
    cmd cabal install $p "$@"
}

ghccmd-armeabi-v7a () {
    stack-push-path
    xport-prepend PATH "$clangpath5_0"
    xport-prepend PATH "$crossbinrootdir"/bin
    xport-prepend PATH "$ghcpath"
    cmd "$ghcarmeabiv7a" "$@"
    stack-pop-path
}

ghcpkgcmd-armeabi-v7a () {
    cmd "$ghcpath"/"$ghcpkgarmeabiv7a" "$@"
}

# --- overcome some quirkiness when installing cairo via cabal, by spoofing
# the `pkg-config` binary and returning reverse-engineered strings.
# --- this trick is only necessary for the two-step ('custom') ones.
# --- how will we know when we can remove this? simple, comment it out and
# see if the build breaks :D
spoof-pkg-config () {
    local target=$1

    cmd redirect-out pkg-config cat <<- EOT
	#!/bin/bash

	set -eu

	arg1="\$1"
	arg2="\${2:-}"

	# echo >debug start
	# echo >>debug "args: $@"

	spoofed=

	if [ "\$arg1" = --modversion ]; then
		if [ "\$arg2" = cairo -o "\$arg2" = cairo-pdf -o "\$arg2" = cairo-svg -o "\$arg2" = cairo-ps ]; then
			echo -n "$cairomodver"
			spoofed=yes
		fi
		if [ "\$arg2" = cairo -o "\$arg2" = cairo-pdf -o "\$arg2" = cairo-svg -o "\$arg2" = cairo-ps ]; then
			echo -n "$cairomodver"
			spoofed=yes
		fi
	elif [ "\$arg1" = --libs ]; then
		if [ "\$arg2" = cairo -o "\$arg2" = cairo-pdf -o "\$arg2" = cairo-svg -o "\$arg2" = cairo-ps ]; then
			echo -n "-L$jnidir/cairo/$target"
			spoofed=yes
		fi
	elif [ "\$arg1" = --cflags ]; then
		if [ "\$arg2" = cairo -o "\$arg2" = cairo-pdf -o "\$arg2" = cairo-svg -o "\$arg2" = cairo-ps ]; then
			echo -n "-I$jnidir/cairo"
			spoofed=yes
		fi
	fi

	if [ "\$spoofed" != yes ]; then
		"$pkgconfigreal" "\$@"
	fi
	EOT

    cmd chmod +x pkg-config

}

spoof-pkg-config-arm () {
    fun spoof-pkg-config "$target_arm"
}

cabal-it () {
    stack-push-path

    xport-prepend PATH "$ghcpath"
    xport-prepend PATH "$clangpath5_0"
    xport-prepend PATH "$crossbindir"

    stack-push-xport LD "$crossbinrootdir"/bin/armv7-linux-androideabi-ld.gold

    if [ "$1" = init -o "$1" = update -o "$1" = sandbox ]; then
        cmd cabal "$@"
    else
        mci
        mcb cabal
        # --- find the last arg which doesn't begin with a -: that's the
        # package -- ugly but works.
        local pack
        local p
        for p in "$@"; do
            if [[ "$p" =~ ^- ]]; then
                continue
            fi
            pack="$p"
        done

        mcb     --builddir="$builddir"
        mcb     --with-ghc="$ghcarmeabiv7a"
        mcb     --ghc-options=-fPIC
        mcb     --with-ghc-pkg="$ghcpkgarmeabiv7a"
        mcb     --with-gcc="$clangarmeabiv7a"
        mcb     --gcc-option=-fPIC
        mcb     --with-strip="$striparmeabiv7a"
        mcb     --with-ld="$ldarmeabiv7a"
        mcb     --hsc2hs-options='--cross-compile -cflag=-fPIC'
        mcb     --package-db=clear
        mcb     --package-db=global
        mcb     --package-db="$packageconfd"
        mcb     --extra-include-dirs="$gluincludedir"
        mcb     --extra-lib-dirs="$builtlibsdirsource"/"$target_arm"
        mcb     --extra-lib-dirs="$glulibdir"
        mcb     --extra-include-dirs="$sdldir/include"
        mcb     "$@"
        mcg
    fi

    stack-pop LD
    stack-pop-path
}

cabalit-have-package () {
    local ret="$1"
    local pack="$2"
    shift

    mci
    mcb cmd-capture _ret0
    mcb   ghcpkgcmd-armeabi-v7a
    mcb   --package-db="$packageconfd"
    mcb   list
    mcg

    local output=$_ret0

    cmd-capture _ret0 basename "$pack"
    local basename=$_ret0

    fun=
    if $fun redirect-in-val "$output" grep -q "$basename"; then
        printf -v "$ret" yes
    else printf -v "$ret" no; fi
}

# --- allow-newer: ignore upper bound constraints on packages; otherwise
# we can't build anything because our compiler is too bleeding-edge.
# --- it seems allow-newer has mysteriously disappeared in two-step builds.

cabal-install-cross () {
    local allow_newer=$1; shift
    local pack
    local allow_newer_flag=
    if [ "$allow_newer" = yes ]; then
        allow_newer_flag=--allow-newer
    fi
    for pack in $@; do
        flags=
        # --- xxx
        if [[ "$pack" =~ OpenGLRaw ]]; then
            flags='--flags=OsAndroid UseGLES2'
        fi

        cabalit-have-package _ret0 "$pack"
        if [ "$_ret0" = yes ]; then
            infof "package %s already installed, ok." "$(green "$pack")"
            continue
        fi

        chd "$hsdir"
        if [ -z "$flags" ]; then
            cmd cabal-it install $allow_newer_flag "$pack"
        else
            cmd cabal-it install $allow_newer_flag "$flags" "$pack"
        fi
    done
}

cabal-install-cross-no-allow-newer () {
    fun cabal-install-cross no "$@"
}

cabal-install-cross-allow-newer () {
    fun cabal-install-cross yes "$@"
}

# --- some packages contain a two-step 'custom' build process, which
# consists of compiling a Setup.(l)hs file based on Distribution.Simple
# (always?), then running the binary.
# --- the problem is that the first step is built with the cross-compiler,
# and can of course not then be executed.
# --- so here we try to break down the steps: first use ordinary ghc, then
# pass the --with-xxx options to the compiled binary.

cabal-install-cross-one-custom () {
    local allow_newer=$1; shift
    local pack=$1
    local allow_newer_flag=
    local f
    local g="Setup.hs Setup.lhs"
    local tmpparts
    local tmp

    stack-push-path

    # --- not updating LDFLAGS flags -- don't seem to be necessary.

    xport-prepend PATH .
    xport-prepend PATH "$ghcpath"
    xport-prepend PATH "$clangpath5_0"
    xport-prepend PATH "$crossbindir"

    if [ "$allow_newer" = yes ]; then
        allow_newer_flag=--allow-newer
    fi

    tmpparts=("${tmptmpdirparts[@]}")
    tmpparts+=(cabal)
    fun safe-rm-dir-array-allow-absolute tmpparts
    tmp=$(join-out / tmpparts)
    mkchd "$tmp"

    # --- if the package name begins with a slash, it's one of our manual
    # overrides: look for it on the filesystem.
    if [[ "$pack" =~ ^/ ]]; then
        cmd cp -ar "$pack" .
        chd "$(basename "$pack")"
    # --- otherwise take it from hackage.
    else
        cmd cabal get "$pack" -d .
        chd "$pack"
    fi
    for f in $g missing; do
        if [ "$f" = missing ]; then error "Can't find any of $g"; fi
        if [ ! -f "$f" ]; then continue; fi
        cmd ghc "$f"
        break
    done

    fun spoof-pkg-config-arm

    # --- the incantation is: package-db clear, then global, then the
    # sandbox.
    # global so it finds base etc.
    # user must not be in there, because packages will interfere and make
    # everything crash and it's really frustrating.
    # global must be there so it can find base etc.
    # also your global should be as minimal as possible: only essential libs
    # (base etc.).
    # finally, --prefix, to install it to the right place.

    # --- xxx
    # --- xxx arm hardcoded
    # --- xxx what is GLURaw-2.0.0.3... for?
    cmd ./Setup \
        --builddir="$builddir" \
        --with-ghc="$ghcarmeabiv7a" \
        --ghc-options=-fPIC \
        --with-ghc-pkg="$ghcpkgarmeabiv7a" \
        --with-gcc="$clangarmeabiv7a" \
        --gcc-option=-fPIC \
        --with-strip="$striparmeabiv7a" \
        --with-ld="$ldarmeabiv7a" \
        --hsc2hs-options='--cross-compile -cflag=-fPIC' \
        --package-db=clear \
        --package-db=global \
        --package-db="$hsdir"/.cabal-sandbox/arm-linux-android-ghc-8.3.20171120-packages.conf.d \
        --extra-include-dirs="$jnidir"/cairo \
        --extra-lib-dirs="$jnidir"/cairo/"$target_arm","GLURaw-2.0.0.3..." \
        --datadir="$hsdir"/.cabal-sandbox/share \
        --prefix="$hsdir"/.cabal-sandbox \
        configure

# --allow-newer mysteriously stopped working.
#--allow-newer

    # --- the builddir has to match, because that's where configure stored
    # its state.
    cmd ./Setup \
        --builddir="$builddir" \
        build
    cmd ./Setup \
        --builddir="$builddir" \
        install

    stack-pop-path

    chd-back-n 2
}

cabal-install-cross-custom-allow-newer () {
    local pack
    for pack in $@; do
        cabalit-have-package _ret0 "$pack"
        if [ "$_ret0" = yes ]; then
            infof "package %s already installed, ok." "$(green "$pack")"
            continue
        fi
        fun cabal-install-cross-one-custom yes "$pack"
    done
}

# --- assemble a find command and return the number of matches.
# --- can't really think of a good way to avoid doing the find twice.
# storing to an intermediate var is hard, also with read.

find-count () {
    local ret="$1"; shift
    info "find \"$@\" -print0 | tr -cd '\0' | wc -c"
    local num
    num=$( ( find "$@" -print0 || true ) | tr -cd '\0' | wc -c)
    read -d '' "$ret" <<< "$num" || true
}

copy-array () {
    local tgtname=$1
    local srcname=$2
    eval-it '%s=("${%s[@]}")' "$tgtname" "$srcname"
    echo "every ${lines[*]}"
}

