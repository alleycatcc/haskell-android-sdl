#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..

. "$bindir"/functions.bash

USAGE="Usage: $0 [-C for no docker cache]"

opt_C=
while getopts hC-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        C) opt_C=yes ;;
        -)  OPTARG_KEY="${OPTARG%=*}"
            OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG_KEY in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

imagename=alleycatcc/haskell-android-sdl

build () {
    local nocache=$1
    local nocacheopt=''
    if [ "$nocache" = yes ]; then
        nocacheopt=--no-cache
    fi
    cmd docker build $nocacheopt -t "$imagename" "$rootdir"
}

fun build "$opt_C"
